# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    # create an empty list
    # check if it is a workday
    # if true, add laptop to list
    # inside that check, also check if it's sunny
    # if false, add umbrella to list
    # if it's not a workday, add surfboard to list
    # return list
    gear = []
    if is_workday == True:
        gear.append("laptop")
        if is_sunny == False:
            gear.append("umbrella")
    else:
        gear.append("surfboard")
    return gear

print(gear_for_day(True, True))
print(gear_for_day(True, False))
print(gear_for_day(False, True))
print(gear_for_day(False, False))
