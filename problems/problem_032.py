# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_numbers(limit):
    # Create a variable to hold the sum
    total = 0
    # Create a list to hold the numbers to add
    num_list = []
    # Check if the number is negative
    # If so, return None
    if limit < 0:
        return None
    # If not, create a list from a range
    # Also append the limit to the end of the range
    else:
        num_list = list(range(limit))
        num_list.append(limit)
    # Then iterate over the list, adding each value sequentially to the sum
    for item in num_list:
        total += item
    # Return the sum
    return total
print(sum_of_first_n_numbers(-1))
print(sum_of_first_n_numbers(0))
print(sum_of_first_n_numbers(1))
print(sum_of_first_n_numbers(2))
print(sum_of_first_n_numbers(5))
