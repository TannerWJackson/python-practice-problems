# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def fizzbuzz(number):
    # check if a number is divisible by 5 and by 3
    # return "fizzbuzz" if so
    if (number % 5) == 0 and (number % 3) == 0:
        return "fizzbuzz"
    # If not, check if it is divisible by 3
    # return "fizz"
    elif (number % 3) == 0:
        return "fizz"
    # If not, check if it is divisible by 5
    # return "buzz"
    elif (number % 5) == 0:
        return "buzz"
    # Otherwise, return the number
    else:
        return number
print(fizzbuzz(15))
print(fizzbuzz(9))
print(fizzbuzz(10))
print(fizzbuzz(13))
