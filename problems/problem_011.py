# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_5(number):
    # Use the modulo operator to find the remander
    # If the remainder is > 0, return the number, otherwise return "buzz"
    if number % 5 > 0:
        return number
    else:
        return "buzz"

print(is_divisible_by_5(10))
print(is_divisible_by_5(7))
