# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    # Create a variable to hold each of the following
        # How many lowercase letters
        # How many uppercase letters
        # How many digits
    # Check if $, !, or @ are present
    # Check the length of the string to see if it's between 6 and 12 characters
    # Iterate over the string
        # Increment the counters according to what they're counting using the .is functions
    # If special characters are present, the length is right, AND the counters are all above zero, return true
    # Else, return false
    lowercase = 0
    uppercase = 0
    digits = 0
    special_chars = False
    length_is_good = False

    if "$" in password or "!" in password or "@" in password:
        special_chars = True

    if len(password) >= 6 and len(password) <= 12:
        length_is_good = True

    for letter in password:
        if letter.isupper():
            uppercase += 1
        if letter.islower():
            lowercase += 1
        if letter.isdigit():
            digits += 1

    if lowercase > 0 and uppercase > 0 and digits > 0 and special_chars == True and length_is_good == True:
        return "Password is good!"
    else:
        return "Password is bad!"

print(check_password("@bC123"))
print(check_password("@bC12"))
