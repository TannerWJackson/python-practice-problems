# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    # Create a variable to hold the list of numbers
    # Create a variable to hold the sums
    # Create a variable to hold the output
    list_of_numbers = []
    output = []
    total = 0
    # Iterate over the list
    for item in csv_lines:
    # For each CSV, split the string into a list of numbers and store it
        list_of_numbers = item.split(",")
        for item in list_of_numbers:
            total += int(item)
        output.append(total)
        total = 0
    return output
    # Add up the numbers in the list
    # Append the sum to the output
    # Return the output

print(add_csv_lines([]))
print(add_csv_lines(["3", "1,9"]))
print(add_csv_lines(["8,1,7", "10,10,10", "1,2,3"]))
