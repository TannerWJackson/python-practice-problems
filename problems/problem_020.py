# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    # divide the members list's value in half
    # see if the attendees list is greater than or equal to the result
    # return true if so
    # return false if not
    if attendees_list >= (members_list / 2):
        return True
    else:
        return False

print(has_quorum(50,100))
print(has_quorum(50,106))
print(has_quorum(50,50))
