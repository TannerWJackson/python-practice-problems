# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    # check if all three ingredients are present
    if "flour" in ingredients and "eggs" in ingredients and "oil" in ingredients:
        # return true if yes
        return True
    else:
        # return false if not
        return False

print(can_make_pasta(["flour","eggs","oil"]))
print(can_make_pasta(["oil","flour","eggs"]))
print(can_make_pasta(["flour","sugar","oil"]))
