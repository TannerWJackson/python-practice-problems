# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    # check if x is greater than or equal to zero AND less than or equal to ten
    # check if y is greater than or equal to zero AND less than or equal to ten
    # return true if both are true or false if one is false
    if x >= 0 and x <= 10 and y >= 0 and y <= 10:
        return True
    else:
        return False

print(is_inside_bounds(10,5))
print(is_inside_bounds(11,5))
print(is_inside_bounds(5,10))
print(is_inside_bounds(5,11))
