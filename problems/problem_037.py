# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    # Convert the number to a string
    # Store that in a variable
    num_str = str(number)
    # Subtract the length of the number from the desired length
    # Store that in a variable
    pad_repeats = length - len(num_str)
    # If the result is negative
    if pad_repeats < 0:
        # Return the number-string
        return num_str
    # If it is not negative
    else:
        # Return the pad the requisite number of times
        # Return the number-string
        return str(pad * pad_repeats) + num_str

print(pad_left(10,4,"*"))
print(pad_left(1000,3,0))
