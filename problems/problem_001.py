# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    # set a variable to hold the output
    min_val = 0
    # compare the arguments
    # set the variable to the smaller of the two
    if value1 >= value2:
        min_val = value2
    else:
        min_val = value1
    # return the variable
    return min_val

print(minimum_value(1,5))
print(minimum_value(8,5))
print(minimum_value(5,5))
