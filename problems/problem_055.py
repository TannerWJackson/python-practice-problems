# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman(number):
    if number >= 1 and number <= 3:
        return ("I" * number)
    elif number == 4:
        return "IV"
    elif number >= 5 and number <=8:
        return "V" + ("I" * (number - 5))
    elif number == 9:
        return "IX"
    elif number == 10:
        return "X"
    else:
        return "Error"

print(simple_roman(1))
print(simple_roman(2))
print(simple_roman(3))
print(simple_roman(4))
print(simple_roman(5))
print(simple_roman(6))
print(simple_roman(7))
print(simple_roman(8))
print(simple_roman(9))
print(simple_roman(10))
