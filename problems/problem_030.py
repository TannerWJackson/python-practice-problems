# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    # Declare a list to hold the values sorted from smallest to largest
    sorted_values = sorted(values)
    # Check if the list is empty
    if values == []:
    # If so, return None
        return None
    # If not, check if the list has only one value
    elif len(values) == 1:
    # If so, return None
        return None
    # If not, grab the second-to-last item in the list using the index minus two
    else:
    # Return that number
        return sorted_values[len(sorted_values) - 2]

print(find_second_largest([]))
print(find_second_largest([1]))
print(find_second_largest([3,4,5]))
print(find_second_largest([-3,-4,-5]))
