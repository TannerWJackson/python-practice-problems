# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    # check if x is between rect_x and (rect_x + rect_width)
    # set a variable to hold the results
    # check if y is between rect_y and (rect_y + rect_height)
    # set another variable to hold the results
    x_in_bounds = False
    y_in_bounds = False

    if x >= rect_x and x <= (rect_x + rect_width):
        x_in_bounds = True
    if y >= rect_y and y <= (rect_y + rect_height):
        y_in_bounds = True
    # compare the variables
    if y_in_bounds == True and x_in_bounds == True:
        # if both true then true
        return True
    else:
        # if one or more is false, then false
        return False

print(is_inside_bounds(5,5,0,0,10,10))
print(is_inside_bounds(5,15,0,0,10,10))
print(is_inside_bounds(15,5,0,0,10,10))
