# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(word):
    result = ""
    for letter in word:
        unicode_index = ord(letter)
        if (unicode_index >= 65 and unicode_index <= 89) or (unicode_index >= 97 and unicode_index <=121):
            result += chr(unicode_index + 1)
        if unicode_index == 122:
            result += chr(97)
        if unicode_index == 90:
            result += chr(65)
    return result

print(shift_letters("import"))
print(shift_letters("Zap"))
