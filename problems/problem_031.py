# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_squares(values):
    # Create a new list to hold the squared values
    values_squared = []
    # Create a new variable to hold the sum of the squares
    total = 0
    # Iterate over values and add the square of each item to the new list
    if values == []:
        return None
    else:
        for item in values:
            values_squared.append(item ** 2)
        # Iterate over the new list adding each value to the sum of the squares
        for item in values_squared:
            total += item
        # Return the sum of the squares
        return total

print(sum_of_squares([]))
print(sum_of_squares([1,2,3]))
print(sum_of_squares([-1,0,1]))
