# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    total = 0
    if values == []:
        return None
    else:
        for item in values:
            total += item
        return total / len(values)

print(calculate_average([10, 20, 30, 40]))
print(calculate_average([1,2,3]))
