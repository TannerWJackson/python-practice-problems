# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.

def find_indexes(search_list, search_term):
    # Create an empty list for the result
    # Create an enumerated list and store it
    # Iterate over the list, looking for the search term
    # If the search term occurs, record the item paired with it (the index of the original lst)
    # Append that item to the result

    result = []
    enum_list = enumerate(search_list)
    for item in enum_list:
        if search_term == item[1]:
            result.append(item[0])
    return result

print(find_indexes([1, 2, 3, 4, 5], 4))
print(find_indexes([1, 2, 3, 4, 5], 6))
print(find_indexes([1, 2, 1, 2, 1], 1))
