# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(x):
    fraction_list = []
    fraction_string = ""
    for value in range(x):
        fraction_list.append(str(value + 1) + "/" + str(value + 2))
    for item in fraction_list:
        fraction_string = " + ".join(fraction_list)
    return fraction_string

print(sum_fraction_sequence(1))
print(sum_fraction_sequence(2))
print(sum_fraction_sequence(3))
