# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    # Create a list of the keys and the values in the dictionary
    reversed_dict = {}
    key_and_values = dictionary.items()
    # Iterate over the list
    for item in key_and_values:
        # Declare each former value a key with the corresponding new value
        # Store that in a variable
        reversed_dict[item[0]] = item[1]
    # Return that new dictionary
    return reversed_dict

print({})
print({"Key": "Value"})
print({"One": "1"})
