# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    # Check if list is empty
    if values == []:
    # If so, return None
        return None
    # If not, sort list from smallest to largest
    sorted_values = sorted(values)
    # Grab the last item in the list using the length of the list minus one as the index
    # Return that item
    return sorted_values[len(sorted_values) - 1]

print(max_in_list([5,4,3,2,1]))
print(max_in_list([5,8,9,2,1]))
print(max_in_list([5,10,10,2,1]))
print(max_in_list([]))
