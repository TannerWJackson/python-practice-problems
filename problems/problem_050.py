# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

# Create variables to hold both results
# Get the length of the initial list
# Divide the length by 2 and floor the result
# Add the items in the initial list in the range outlined above to result 1
# Add the remaining items to result 2
import math
def halve_the_list(list):
    result_one = []
    result_two = []
    index_of_one = math.ceil(len(list) / 2)

    for index in range(index_of_one):
        result_one.append(list[index])

    for index in range(index_of_one, len(list)):
        result_two.append(list[index])

    return result_one, result_two

print(halve_the_list([1, 2, 3, 4]))
print(halve_the_list([1, 2, 3]))
