# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_even_numbers(n):
    # Create a variable to hold the sum
    total = 0
    # Create a variable to hold the numbers to add
    range_of_n = []
    range_of_n_doubled = []
    # Check if the number is negative
    # If yes, return None
    if n < 0:
        return None
    # If no, then create a list from the range of n
    else:
        range_of_n = list(range(n))
        # Append n to the list
        range_of_n.append(n)
        # Multiply each item in the list by 2
        for item in range_of_n:
            range_of_n_doubled.append(item * 2)
        # Add each item in the list to the total
        for item in range_of_n_doubled:
            total += item
        return total

print(sum_of_first_n_even_numbers(-1))
print(sum_of_first_n_even_numbers(0))
print(sum_of_first_n_even_numbers(1))
print(sum_of_first_n_even_numbers(2))
print(sum_of_first_n_even_numbers(5))
