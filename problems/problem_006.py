# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    #Create variable to store result
    eligibility = ""
    #Check if age >= 18 AND that they have a consent form
    if age >= 18 and has_consent_form == True:
        eligibility = "You can skydive!"
    elif age >= 18 and has_consent_form == False:
        eligibility = "Come back when you've signed the waiver."
    else:
        eligibility = "Come back in a few years."
    #Return variable
    return eligibility

print(can_skydive(16, True))
print(can_skydive(16, False))
print(can_skydive(19, True))
print(can_skydive(19, False))
