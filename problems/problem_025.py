# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    # Declare a variable to hold the sum
    total = 0
    # check if list is empty
    if values == []:
    # if so, return None
        return None
    # if not, add each item in the list to the sum by iterating over the list
    else:
        for item in values:
            total += item
    # return the sum
        return total

print(calculate_sum([]))
print(calculate_sum([1,2,3]))
print(calculate_sum([1,2,3,4,5]))
