# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    # Set a variable to hold the max result
    max_val = 0
    # compare the first value to the second and third
    if value1 >= value2 and value1 >= value3:
        max_val = value1
   # if it's not biggest, compare the second value to the third
    elif value2 >= value3:
        max_val = value2
    else:
        max_val = value3
    # set the variable to the biggest number
    # return the variable
    return max_val

print(max_of_three(1,2,3))
print(max_of_three(8,7,6))
print(max_of_three(6,8,7))
