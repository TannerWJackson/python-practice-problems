# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

# Create a variable to store the differences
# Iterate over the list, starting at index 1
    # Subtract each number from the previous one
    # Get the absolute value
    # Append that value to the list
# Sort the list from smallest to largest
# Grab the last value and return it

def biggest_gap(num_list):
    differences_list = []
    for index in range(1, len(num_list)):
        differences_list.append(abs(num_list[index] - num_list[index - 1]))
    differences_list.sort()
    return differences_list[len(differences_list) - 1]

print(biggest_gap([1, 11, 9, 20, 0]))
